
//4 Create a fetch request using the GET methodthat will retrieve all the to do list items from JSON placeholder API

//5 Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json)=> { let todolist = json.map((todo => {return todo.title}))
  console.log(todolist);
});




//6 Create a fetch request using the GET method that will retrieve a single todo list item from JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/68')
.then((response) => response.json())
.then((json)=> console.log(json))

//7 Using the data retrieved , print a message in the console that will provide the title and status of the todo list item
fetch('https://jsonplaceholder.typicode.com/todos/70')
.then((response) => response.json())
.then((json)=> console.log(`TITLE:${json.title} and STATUS: ${json.pending}` ))
//8 Create a fetch request using POST method that will create a todo list item using the JSON Placeholder API

fetch ('https://jsonplaceholder.typicode.com/todos',{
  method: 'POST',
  headers:{
    'Content-Type' : 'application/json'
  },
  body: JSON.stringify({
    title: 'New to do',
    body: 'I am a new to do',
    userId:1
  })

})
.then((response) => response.json())
.then((json) => console.log(json));


//Create a fetch request using the PUT method that will update a todo list item using the  JSON placeholder API
// Update a todo list item by changing the data structure to contain the following properties
//a. Title
//b. Description
//c. Status
//d. Date completed
//e/ User ID
fetch('https://jsonplaceholder.typicode.com/todos/4',{method:'PUT', headers: {'Content-Type': 'application/json'}, body: JSON.stringify({id:1, title: 'New TODO', description: 'Updating todo', status: 'complete' , dateCompleted: 'Jan 5', userID: 1 })})
.then((response)=> response.json())
.then((json) => console.log(json));




//11 Create  a fetch request using the PATCH method that will update a todo list item using the JSON placeholder API
//12 Update a todo list item by changing the status to complete and add a date when the status was changed
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    status: 'complete',
    date: 'Jan 6'
  })
})
.then((response) => response.json())
.then((json) => console.log(json))


	
//13 Create a fetch request using the DELETE method that will delete an item using the DELETE method that will delete an item using the JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/15', {
  method: 'DELETE'
})
//14 Create a request via Postman to retrieve all the todo list items
//a GET HTTP method
//b https://jsonplaceholder.typicode.com/todos/1 URL endpoint
//c Save this request as get all to do list items ---OK



//15 Create a request via Postman to retrieve an individual todo list
//a. POST HTTP method
//b https://jsonplaceholder.typicode.com/todos  URL endpoint
//c Save this request as create todo list item --- OK



//16 Create a request via Postman to update a todo list item
//a. PUT HTTP method
//b https://jsonplaceholder.typicode.com/todos/1
//c. Save this request  as update to do list item PUT
//d. Update the todo list item to mirror the data structure used in the PUT fetch request --OK


//17 Create a request via Postman to update a todo list item
//a. Patch HTTP method
//b. https://jsonplaceholder.typicode.com/todos/1 URL endpoint
//c. Save this request as create to do list item
//d. Update the todo list item to mirror the data structure of the PATCH fetch request  --OK



//18 Create a request via POstman to delete a todo list item
//a. DELETE HTTP method
//b. https://jsonplaceholder.typicode.com/todos/1 URL endpoint
//c. Save this request as Delete to do list item --OK






